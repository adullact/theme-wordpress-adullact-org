<?php
get_header();
?>
    <div class="wrap">
        <?php
        if (is_user_logged_in()) {
            /**
             * Dans le cas où l'utilisateur est connecté, on ajoute du contenu personnalisé.
             */
            echo "<p>Du contenu personnalisé visible uniquement pour les personnes connectées</p>";
        } else {
            /**
             * Dans le cas où l'utilisateur n'est pas connecté, on affiche du contenu personnalisé, ainsi qu'un lien de
             * connexion.
             */
            $redirect_url = $_SERVER ["REQUEST_URI"];
            echo "<a href='" . wp_login_url($redirect_url) . "' class='adullact-blue'>Se connecter pour accéder aux ressources</a>";
        }
        ?>
    </div>
<?php
get_footer();