<?php
require_once "config-site.inc.php";

function my_theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

define("DEBUG_FUNCTIONS", false);

function get_adullact_page($slug, $class = "")
{
    $args = array(
        "post_type" => "page",
        "orderby" => "title",
        "name" => "$slug",
        "posts_per_page" => -1);
    $page = get_posts($args)[0];

    //var_dump($page);
    $result = "<a href='" . $page->guid . "' class='" . $class . "'>" . $page->post_title . "</a>";
    echo $result;
}

function echo_debug($words)
{
    //si mode debug activé, on affiche les informations demandées
    if (DEBUG_FUNCTIONS) {
        echo $words;
    }
}

function dump_debug($list)
{
    //si mode debug activé, on affiche le var dump demandé
    if (DEBUG_FUNCTIONS) {
        var_dump($list);
    }
}

/**on supprime du texte inutile généré sur certaines pages
 * @return string
 */
function wpc_remove_archive_title_prefix()
{
    return "";
}

add_filter('get_the_archive_title', 'wpc_remove_archive_title_prefix');