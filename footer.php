</div><!-- #content -->
<footer id="colophon" class="site-footer" role="contentinfo">
    <div id="adullact-footer" class="wrap">
        <ul>
            <li><?php get_adullact_page("mentions-legales","adullact-underline") ?></li>
            <li><?php get_adullact_page("accessibilite","adullact-underline") ?></li>
            <li><?php get_adullact_page("contact","adullact-underline") ?></li>
            <li><?php get_adullact_page("plan-du-site","adullact-underline") ?></li>
        </ul>

        <p id="adullact-contact-informations">
            Association ADULLACT, 5 Rue du Plan du Palais, 34000 Montpellier<br>
            contact AROBASE adullact.org<br>
            04 67 65 05 88
        </p>
    </div><!-- .wrap -->
</footer><!-- #colophon -->
</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
