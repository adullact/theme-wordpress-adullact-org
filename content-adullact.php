<?php
/**
 * affiche un article seul. il est appelé depuis index.php lors de l'affichage d'une actualité.
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class("adullact-post"); ?>>
    <?php
    if (is_sticky() && is_home()) :
        echo twentyseventeen_get_svg(array('icon' => 'thumb-tack'));
    endif;
    ?>
    <header class="entry-header">
        <?php
        if ('post' === get_post_type()) {
            echo '<div class="entry-meta">';
            if (is_single()) {
                twentyseventeen_posted_on();
            }
            echo '</div><!-- .entry-meta -->';
        };

        if (is_single()) {
            the_title('<h1 class="entry-title">', '</h1>');
        } elseif (is_front_page() && is_home()) {
            the_title('<h3 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>');
        } else {
            the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
        }
        if ( is_single() ) {
            twentyseventeen_posted_on();
        } else {
            echo twentyseventeen_time_link();
            twentyseventeen_edit_link();
        };
        ?>
    </header><!-- .entry-header -->

    <?php if ('' !== get_the_post_thumbnail() && !is_single()) : ?>
        <div class="post-thumbnail">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('post-thumbnail', array("class" => "adullact-post-image")); ?>
            </a>
        </div><!-- .post-thumbnail -->
    <?php endif; ?>

    <div class="entry-content">
        <?php
        the_content("<span class='adullact-read-more'>Lire la suite...</span>");

        wp_link_pages(
            array(
                'before' => '<div class="page-links">' . __('Pages:', 'twentyseventeen'),
                'after' => '</div>',
                'link_before' => '<span class="page-number">',
                'link_after' => '</span>',
            )
        );
        ?>
    </div><!-- .entry-content -->

    <?php
    twentyseventeen_entry_footer()
    ?>

</article><!-- #post-<?php the_ID(); ?> -->
