<?php
/**
 * Adresse mail cible de l'Adullact.
 * Lorsqu'on envoie un mail informatif à l'Adullact, à qui l'envoie-t-on ?
*/
define("MAIL_CIBLE_ADULLACT","nathan.peraldi@adullact.org");

/**
 * Le chemin d'accès aux logs du formulaire d'adhésion
 */
$theme_path = get_stylesheet_directory();
$membership_logs_path = $theme_path . "/logs/adhesion/logs.log";
define("MEMBERSHIP_LOGS_DIRECTORY",$membership_logs_path);