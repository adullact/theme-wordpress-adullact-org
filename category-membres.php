<?php
//nombre max de membres par page
$maxMembersPerPage = 20;

$page = 1;
$order = "asc";
$link = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER["REDIRECT_URL"];
$pagination = true;

/**On affiche les boutons pour trier les membres en fonction du type de collectivité
 * @param string $category_to_select
 */
function childrens_of_member_category($category_to_select)
{
    $member_category_object = get_category_by_slug("membres");

    $id = $member_category_object->cat_ID;
    $server_link = $_SERVER["SERVER_NAME"];
    $link_Category = "http://" . $server_link . "/category/";
    $current_category = $category_to_select;

    dump_debug($_SERVER);
    echo_debug("\n----------------------\n");
    dump_debug($member_category_object);

    $childrens = get_term_children($id, "category"); //contiendra les id de tous les enfants de adhérent

    if ($current_category == "membres" || $current_category == "") {
        echo " <li class='adullact-sort-button adullact-selected'><a href='" . $link_Category . "membres/'>Tout</a></li>";
    } else {
        echo " <li class='adullact-sort-button'><a href='" . $link_Category . "membres/'>Tout</a></li>";
    }

    foreach ($childrens as $c) {
        $temp_category = get_category($c); //objet contenant les infos de la catégorie enfant

        $my_link = "<li class='adullact-sort-button";

        if ($temp_category->slug == $category_to_select) {
            //si on est en train d'afficher la catégorie sélectionnée
            $my_link .= " adullact-selected";
        } else {
            echo_debug($temp_category->slug . "=/=" . $category_to_select);
        }

        $my_link .= "'><a href = \"http://" . $server_link . "/category/";
        $my_link .= $temp_category->slug;
        $my_link .= "\"";

        $my_link .= "\">";
        $my_link .= $temp_category->name;
        $my_link .= "</a></li>";

        echo $my_link;
    }
}

/**on génère les boutons pour trier les membres par ordre croissant / décroissant
 * @param string $order
 * @param string $page
 * @param string $link
 */
function sort_page($order, $page, $link)
{
    $for_asc_link = $link . "?page=" . $page . "&order=asc";
    $for_desc_link = $link . "?page=" . $page . "&order=desc";
    if ($order == "desc") {
        ?>
        <li class="adullact-sort-button">
            <a href="<?php echo $for_asc_link; ?>">
                Croissant
            </a>
        </li>
        <li class="adullact-sort-button adullact-selected">
            <a href="<?php echo $for_desc_link; ?>">
                Décroissant
            </a>
        </li>
        <?php
    } else {
        ?>
        <li class="adullact-sort-button adullact-selected">
            <a href="<?php echo $for_asc_link; ?>">
                Croissant
            </a>
        </li>
        <li class="adullact-sort-button">
            <a href="<?php echo $for_desc_link; ?>">
                Décroissant
            </a>
        </li>

        <?php
    }
}
//on vérifie bien le numéro de la page
if (isset($_GET["page"])) {
    if (is_numeric($_GET["page"])) {
        if (($_GET["page"] > 0) && ($_GET["page"] < 100)) {
            $page = $_GET["page"];
        }
    }
}

//on vérifie bien l'ordre donné
if (isset($_GET["order"])) {
    if ($_GET["order"] == "desc") {
        $order = "desc";
    }
}

get_header();
$min_member_numero = 1 + ($page * $maxMembersPerPage) - $maxMembersPerPage;
$max_member_numero = ($page * $maxMembersPerPage);
?>
    <div class="wrap adullact-no-height-padding">
        <main id="main" class="site-main" role="main">
            <div class="adullact-spaced-line">
                <div id="adullact-sort-by-type-buttons">
                    <ul class="adullact-member-list">
                        <?php
                        global $post; //variable wp
                        $url = explode("/", $_SERVER["REDIRECT_URL"]);

                        //nombre de mots dans l'url, sert à déterminer la catégorie recherchée
                        $number_of_words = count($url);
                        if ($url[$number_of_words - 1] == "") {
                            unset($url[$number_of_words - 1]);
                            $url = array_values($url);
                            $number_of_words -= 1;
                        }
                        $category = $url[$number_of_words - 1];
                        childrens_of_member_category($category);

                        ?>
                    </ul>
                </div>
                <div id="adullact-sort-by-name">
                    <ul class="adullact-member-list">
                        <?php sort_page($order, $page, $link) ?>
                    </ul>
                </div>
            </div>

                <?php
                $args = array(
                    "post_type" => "page",
                    "order" => $order,
                    "category_name" => $category,
                    "orderby" => "title",
                    "posts_per_page" => -1);
                $mypages = get_posts($args);

                $number_of_total_posts = count($mypages); //pour les pages
                if (($number_of_total_posts == 0) || ($number_of_total_posts < $min_member_numero)) {
                    echo "<h3 class='adullact-blue'>Aucun résultat.</h3>";
                    $pagination = false;
                } else {
                    echo "<h3 class='adullact-blue adullact-no-padding'>" . $number_of_total_posts . " résultats</h3>";
                    echo "<div class='adullact-members-list'>";
                    $counter = 1;
                    foreach ($mypages as $post) : setup_postdata($post);
                        if ($counter >= $min_member_numero) {
                            if ($counter <= $max_member_numero) {
                                $id = get_the_id();
                                $url = get_metadata("post", $id, "project_url")[0];

                                ?>
                                <div class="adullact-member">
                                    <div>
                                        <a href="<?php echo $url; ?>" target="_blank">
                                            <?php the_post_thumbnail(array("200", "200")); ?> <br>
                                        </a>
                                    </div>

                                    <p><?php the_title(); ?></p><br>
                                </div>
                                <?php
                            } else {
                                //on a dépassé le nombre max de membres à afficher, on peut donc stopper la boucle car elle n'affichera plus rien.
                                break;
                            }
                        }
                        $counter += 1;
                    endforeach;
                    ?>
                    </div>
                    <?php
                    wp_reset_postdata();
                }

                if ($pagination) {
                    ?>
                    <div class="pagination">
                        <?php

                        $number_of_pages_to_generate = intdiv($number_of_total_posts, $maxMembersPerPage);
                        $reste = $number_of_total_posts % $maxMembersPerPage;

                        if ($reste > 0) {
                            $number_of_pages_to_generate += 1;
                        }

                        for ($i = 1; $i <= $number_of_pages_to_generate; $i++) {
                            $page_link = $link . "?page=" . $i . "&order=" . $order;
                            $link_class = "adullact-page-link";
                            if ($page == $i) {
                                $link_class .= " adullact-page-link-selected";
                            }
                            ?>
                            <a href="<?php echo $page_link ?>" class="<?php echo $link_class ?>"><?php echo $i ?></a>
                            <?php
                        }
                        ?>
                    </div>
                <?php } ?>
        </main>
    </div>
<?php get_footer(); ?>