<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

$theme_name = get_stylesheet_directory_uri();
$twitter_directory = $theme_name . "/media/icons/" . "twitter.png";
$youtube_directory = $theme_name . "/media/icons/" . "youtube.png";
$mastodon_directory = $theme_name . "/media/icons/" . "mastodon.png";
$linkedin_directory = $theme_name . "/media/icons/" . "linkedin.png";
$adullact_logo_directory = $theme_name . "/media/adullact/" . "logo_adullact_small_cut.png";

?>
<div class="site-branding adullact-no-padding">
    <div class="wrap adullact-no-padding adullact-header">
        <?php the_custom_logo(); ?>

        <div class="site-branding-text">
            <?php if (is_front_page()) : ?>
                <h1 class="site-title">
                    <img id="logo-adullact-big" src="<?php echo $adullact_logo_directory ?>" alt="Logo de L'Adullact">
                </h1>
            <?php else : ?>
                <p class="site-title">
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                        <img id="logo-adullact-small" src="<?php echo $adullact_logo_directory ?>"
                             alt="Retour à l'accueil">
                    </a>
                </p>
            <?php endif;
            $description = get_bloginfo('description', 'display');
            if ($description || is_customize_preview()) :
                ?>
                <p class="site-description">
                    <?php echo $description; ?>
                </p>
            <?php endif; ?>
        </div><!-- .site-branding-text -->
        <div class="adullact-searchbar-socialnetworks">
            <div class="adullact-reseaux-sociaux">
                <a href="https://twitter.com/ADULLACT">
                    <img src="<?= $twitter_directory ?>" alt="Twitter de L'Adullact">
                </a>
                <a href="https://mastodon.social/@adullact">
                    <img src="<?= $mastodon_directory ?>" alt="Mastodon de l'Adullact">
                </a>
                <a href="https://fr.linkedin.com/company/adullact">
                    <img src="<?= $linkedin_directory ?>" alt="LinkedIn de l'Adullact">
                </a>
                <a href="https://www.youtube.com/user/ADULLACT">
                    <img src="<?= $youtube_directory ?>" alt="Youtube de l'Adullact">
                </a>
            </div>
            <div class=" adullact-searchbar-parent">
                <div class="adullact-searchbar">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>

        <?php if ((twentyseventeen_is_frontpage() || (is_home() && is_front_page())) && !has_nav_menu('top')) : ?>
            <a href="#content" class="menu-scroll-down">
                <?php echo twentyseventeen_get_svg(array('icon' => 'arrow-right')); ?>
                <span class="screen-reader-text">
                    <?php _e('Scroll down to content', 'twentyseventeen'); ?>
                </span>
            </a>
        <?php endif; ?>

    </div><!-- .wrap -->
</div><!-- .site-branding -->
