<?php
/*
Template Name: Adhésion individuelle
*/
get_header();

?>
    <div class="wrap adullact-no-height-padding">
        <div id="primary" class="content-area">
            <h1 class="adullact-blue">Adhésion individuelle</h1>
            <p>Les adhésions individuelles se font sur le site HelloAsso.</p>
            <a href="https://www.helloasso.com/associations/adullact/adhesions/adhesion-individuelle-a-l-adullact"
               class="adullact-button">Adhérer en tant qu'individu (par le site HelloAsso)</a>
        </div>
    </div>
<?php
get_footer();