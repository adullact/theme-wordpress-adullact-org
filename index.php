<?php
/**
 * The main template file
 * ce fichier a pour objectif d'afficher les actualités, malgré son nom "index.php" qui pourrait laisser penser
 * que c'est la page d'accueil.
 */
$theme_directory = get_stylesheet_directory_uri();
$qui_sommes_nous_link = $_SERVER["SERVER_NAME"] . "/association/qui-sommes-nous/";
$adullact_icons_directory = $theme_directory . "/media/icons/";

$comptoir_icon = $adullact_icons_directory . "comptoir.png";
$faq_icon = $adullact_icons_directory . "faq.png";
$forge_icon = $adullact_icons_directory . "forge.png";
$labelTNL_icon = $adullact_icons_directory . "labelTNL.png";
$services_icon = $adullact_icons_directory . "services.png";
$newsletter_icon = $adullact_icons_directory . "newsletter.png";

get_header(); ?>
    <div id="adullact-mainpage-text">
        <div class="wrap">
            <h2 class="adullact-no-height-padding">L'Adullact en quelques mots</h2>
            <p class="adullact-justify">
                Fondée en 2002, l'ADULLACT (Association des Développeurs et Utilisateurs de Logiciels Libres pour
                les Administrations et les Collectivités Territoriales) a pour objectifs de promouvoir, développer
                et maintenir un patrimoine de logiciels libres utiles aux missions de service public.
            </p>
            <p class="adullact-justify">
                Structure unique en Europe, est une initiative née de la nécessité de voir apparaître une
                alternative au système des licences propriétaires, en particulier dans le domaine des logiciels
                métiers.
            </p>
            <a href="<?php echo $qui_sommes_nous_link ?>" class="">En savoir plus sur L'Adullact</a>
        </div>
    </div>

    <div id="adullact-mainpage-frames">
        <div class="adullact-cadres-column wrap">
            <h2 class="adullact-no-height-padding">Nos initiatives</h2>
            <div class="adullact-cadres-ligne-space">
                <div class="adullact-cadre">
                    <div class="adullact-icone-service">
                        <img src="<?= $comptoir_icon ?>" alt="">
                    </div>
                    <a href="https://comptoir-du-libre.org/">
                        <h3 class="adullact-titre-cadre">Le Comptoir</h3>
                    </a>
                    <p class="adullact-description-cadre">Pour trouver le logiciel libre ou prestataire pour vos
                        besoins
                    </p>
                </div>
                <div class="adullact-cadre">
                    <div class="adullact-icone-service">
                        <img src="<?= $forge_icon ?>" alt="">
                    </div>
                    <a href="https://gitlab.adullact.net/">
                        <h3 class="adullact-titre-cadre">Les Forges</h3>
                    </a>
                    <p class="adullact-description-cadre">Pour trouver ou déposer le code source d’un logiciel
                        libre
                    </p>
                </div>
                <div class="adullact-cadre">
                    <div class="adullact-icone-service">
                        <img src="<?= $services_icon ?>" alt="">
                    </div>
                    <a href="http://www.s2low.org/">
                        <h3 class="adullact-titre-cadre">Les services</h3>
                    </a>
                    <p class="adullact-description-cadre">Pour découvrir les services en ligne offerts à nos
                        adhérents
                    </p>
                </div>

                <div class="adullact-cadre">
                    <div class="adullact-icone-service">
                        <img src="<?= $labelTNL_icon ?>" alt="">
                    </div>
                    <a href="#">
                        <h3 class="adullact-titre-cadre">Le Label TNL</h3>
                    </a>
                    <p class="adullact-description-cadre">Pour récompenser les usages numériques libres dans les
                        collectivités
                    </p>
                </div>
                <div class="adullact-cadre">
                    <div class="adullact-icone-service">
                        <img src="<?= $newsletter_icon ?>" alt="">
                    </div>
                    <a href="#">
                        <h3 class="adullact-titre-cadre">La lettre</h3>
                    </a>
                    <p class="adullact-description-cadre">Pour recevoir les nouvelles de la communauté libre</p>
                </div>
                <div class="adullact-cadre">
                    <div class="adullact-icone-service">
                        <img src="<?= $faq_icon ?>" alt="">
                    </div>
                    <a href="#">
                        <h3 class="adullact-titre-cadre">La FAQ</h3>
                    </a>
                    <p class="adullact-description-cadre">Pour trouver des réponses à vos questions sur le libre</p>
                </div>
            </div>
        </div>
    </div>

    <!-- effet parallax-->
    <div id="adullact-parallax-1">>
        <div class="wrap">
            <div class="adullact-cadres-ligne-center">
                <p class="adullact-cadre-texte">
                    <span class="adullact-front-page-number">342</span>
                    <span>Adhérents directs</span>
                </p>
                <p class="adullact-cadre-texte">
                    <span class="adullact-front-page-number">5200</span>
                    <span>Collectivités touchées</span>
                </p>
                <p class="adullact-cadre-texte">
                    <span class="adullact-front-page-number">17</span>
                    <span>Ans d'expérience</span>
                </p>
            </div>
        </div>
    </div>
    <div id="adullact-mainpage-articles">
        <div class="wrap">
            <h2 class="adullact-no-height-padding">Nos derniers articles</h2>
            <div class="adullact-cadres-ligne-space">
                <?php
                $args = array(
                    "post_type" => "post",
                    "order" => "DESC",
                    "category_name" => "actualite",
                    "orderby" => "date",
                    "posts_per_page" => 3);

                $my_posts = get_posts($args);
                if (count($my_posts) > 0) {
                    foreach ($my_posts as $post) : setup_postdata($post);
                        ?>
                        <div class="adullact-cadre-article">
                            <?php the_post_thumbnail(array("200", "200")) ?>
                            <span><?php echo get_the_date() ?></span>
                            <a href="<?php echo get_permalink($post); ?>">
                                <p><?php the_title() ?></p>
                            </a>
                        </div>
                    <?php
                    endforeach;
                    wp_reset_postdata();
                }

                ?>
            </div>
        </div>
    </div>
    <div id="adullact-mainpage-pseudofooter">
        <div class="adullact-cadres-column wrap">
            <div class="adullact-cadres-ligne-space">

                <div id="adullact-cadre-twitter">

                </div>
                <div id="adullact-cadre-openstreetmap">

                </div>
                <div id="adullact-cadre-contact">

                </div>
            </div>
        </div>

    </div>

<?php
get_footer();
