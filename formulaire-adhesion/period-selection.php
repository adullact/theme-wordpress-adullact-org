<?php

//ce fichier contient les fonctions nécessaires pour générer 13 <option> </option>
//ces options sont utilisées pour sélectionner une période d'un an, 13 choix sont disponibles

function actualMonthYear($plus = 0)
{
    //retourne un string : le numéro du mois actuel auquel on ajoute $plus et les années à rajouter en fonction
    //sous la forme MM/01/YY

    $month = date("m");
    $year = date("y");
    $month = $month + $plus;

    while ($month > 12) {
        $month -= 12;
        $year += 1;
    }
    $month = addZero($month);
    $year = addZero($year);
    return $month . "/01/" . $year;
}

function addZero($number)
{
    //ajoute un zéro à $number s'il le faut
    //exemple : addZero("1") retourne "01"

    if (strlen($number) == 1) {
        return "0" . $number;
    }
    return $number;
}

function generateDates()
{
    //cette fonction génère les options pour la question "période d'adhésion"
    for ($i = 0; $i < 13; $i++) {
        $premiereDate = new DateTime(actualMonthYear($i));

        $annee = new DateInterval("P1Y");
        $jour = new DateInterval("P1D");
        $message = "du " . $premiereDate->format("d/m/y") . " au ";
        $premiereDate->add($annee);
        $premiereDate->sub($jour);

        $message = $message . $premiereDate->format("d/m/y");
        $message = "<option class='optionPeriod' value='" . $message . "'>" . $message . "</option>";

        echo $message;
    }
}
