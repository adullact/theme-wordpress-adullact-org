<?php
/*
Template Name: Formulaire d'adhésion
*/

require_once "formulaire-adhesion-functions.php";
get_header();

?>
    <div class="wrap adullact-no-height-padding">
        <div id="primary" class="content-area">
            <?php
            if ($isFormulaireValide) {
                ?>
                <h1 class="adullact-title">Formulaire de renouvellement d'adhésion</h1>
                <p>La demande de renouvellement d'adhésion a bien été prise en compte. Nous allons vous recontacter sous
                    peu.</p>
                <?php
            } else {
                ?>
                <main id="main" class="site-main" role="main">
                    <form action="" method="post" id="adullact-membership-form">
                        <h1 class="adullact-title">Formulaire de renouvellement d'adhésion</h1>
                        <p>Les champs portant une astérisque
                            <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                            sont obligatoires
                        </p>

                        <!-- Pot de miel -->
                        <label for="adullact-potdemiel"></label>
                        <input type="checkbox" id="adullact-potdemiel" name="adullact_validation_checkbox[]" value="1">

                        <?php
                        if ($numberOfErrors == 1) {
                            echo "<p class='adullact-error'>Une erreur est survenue lors de l'envoi du formulaire. Veuillez vérifier vos informations.</p>";
                        } elseif ($numberOfErrors > 1) {
                            echo "<p class='adullact-error'>Plusieurs erreurs sont survenues lors de l'envoi du formulaire. Veuillez vérifier vos informations.</p>";
                        }
                        ?>

                        <!--premières infos qui vont servir pour js-->
                        <input id="ancienSelectType" type="hidden" name="ancienSelectType"
                               value="<?php tryToEcho("firstSelection",$isFormulaireValide) ?>">
                        <input id="ancienSelectSize" type="hidden" name="ancienSelectSize"
                               value="<?php tryToEcho("secondSelection",$isFormulaireValide) ?>">
                        <input id="ancienSelectPeriod" type="hidden" name="ancienSelectPeriod"
                               value="<?php tryToEcho("periodeAdhesionCollectivite",$isFormulaireValide) ?>">

                        <label for="firstSelect">
                            <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                            Veuillez sélectionner le type de collectivité :
                        </label>

                        <select id="firstSelect" required="" name="firstSelection">
                            <option value="">---</option>

                            <?php
                            //on génère les options en fonction du fichier et des valeurs par défaut.
                            for ($i = 0; $i < count($textsSelectMenuExtended); $i++) {
                                ?>
                                <option class="optionFirstSelect"
                                        value="<?php echo $classSelectMenuExtended[$i] ?>"><?php echo $textsSelectMenuExtended[$i] . " " . $descriptions[$textsSelectMenuExtended[$i]] ?></option>
                                <?php
                            }
                            ?>
                        </select>

                        <?php tryToShowError($errors,"first selection",$isFormulaireValide); ?>
                        <div id="link">
                            <a href="formulaire-adhesion.php" id="linkRedirection">Texte</a>
                        </div>

                        <div id="formulaireEPCIMutualisant">
                            <label for="nombreHabitantsEPCI">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Nombre d'habitants de l'EPCI :
                            </label>
                            <input type="number" name="nombreHabitantsEPCI" id="nombreHabitantsEPCI"
                                   value="<?php tryToEcho("nombreHabitantsEPCI",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"EPCI hab",$isFormulaireValide); ?>

                            <label for="nombreCommunesMembresEPCI">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Nombre de communes membres :
                            </label>
                            <input type="number" name="nombreCommunesMembresEPCI" id="nombreCommunesMembresEPCI"
                                   value="<?php tryToEcho("nombreCommunesMembresEPCI",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"EPCI com",$isFormulaireValide); ?>
                        </div>

                        <div id="paraSecondSelection">
                            <?php
                            //on génère les labels
                            generateLabelCotisations($listeGrilleEntiere); ?>
                            <select id="secondSelection" name="secondSelection">
                                <?php
                                //et on génère les options qui seront cachées plus tard
                                generateSecondSelection($listeGrilleEntiere); ?>
                            </select>
                            <?php tryToShowError($errors,"second Selection",$isFormulaireValide); ?>
                        </div>

                        <div id="cotisation">
                            <?php generateCotisations($listeGrilleEntiere); ?>
                            <p id="PCOT-EPCIMutualisant" class="PCOT">Montant de la cotisation : <span
                                        id="montantCotisation"></span>€</p>
                            <p id="PCOT-Association" class="PCOT">Montant de la cotisation : 15€</p>
                        </div>
                        <div id="informationsCollectivite">
                            <label for="periodeAdhesionCollectivite">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Période d'adhésion :
                            </label>
                            <select name="periodeAdhesionCollectivite" id="periodeAdhesionCollectivite" required="">
                                <option class="optionPeriod" value="" selected>---</option>
                                <?php generateDates(); ?>
                                <option class="optionPeriod" value="Autre" selected>Autre : nous vous recontacterons.
                                </option>
                            </select>
                            <?php tryToShowError($errors,"periode",$isFormulaireValide); ?>

                            <h3>Personne morale :</h3>

                            <label for="nomCollectivite">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Nom de la collectivité :
                            </label>
                            <input type="text" name="nomCollectivite" id="nomCollectivite" required=""
                                   value="<?php tryToEcho("nomCollectivite",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"nom",$isFormulaireValide); ?>

                            <label for="adresseCollectivite">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Adresse :</label>
                            <input type="text" name="adresseCollectivite" id="adresseCollectivite" required=""
                                   value="<?php tryToEcho("adresseCollectivite",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"adresse",$isFormulaireValide); ?>

                            <label for="codePostalCollectivite">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Code postal :</label>
                            <input type="text" name="codePostalCollectivite" id="codePostalCollectivite" required=""
                                   value="<?php tryToEcho("codePostalCollectivite",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"CP",$isFormulaireValide); ?>

                            <label for="villeCollectivite">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Ville :</label>
                            <input type="text" name="villeCollectivite" id="villeCollectivite" required=""
                                   value="<?php tryToEcho("villeCollectivite",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"ville",$isFormulaireValide); ?>

                            <label for="telCollectivite">Téléphone :</label>
                            <input type="tel" name="telCollectivite" id="telCollectivite"
                                   value="<?php tryToEcho("telCollectivite",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"tel",$isFormulaireValide); ?>

                            <label for="siteCollectivite">Site internet :</label>
                            <input type="url" name="siteCollectivite" id="siteCollectivite"
                                   value="<?php tryToEcho("siteCollectivite",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"site",$isFormulaireValide); ?>

                            <h3>Personne physique / représentant de la personne morale :</h3>

                            <label for="nomUser">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Nom de famille :
                            </label>
                            <input type="text" name="nomUser" id="nomUser" required=""
                                   value="<?php tryToEcho("nomUser",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"nomUser",$isFormulaireValide); ?>

                            <label for="prenomUser">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Prénom :
                            </label>
                            <input type="text" name="prenomUser" id="prenomUser" required=""
                                   value="<?php tryToEcho("prenomUser",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"prenomUser",$isFormulaireValide); ?>

                            <label for="fonctionUser">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Fonction :
                            </label>
                            <input type="text" name="fonctionUser" id="fonctionUser" required=""
                                   value="<?php tryToEcho("fonctionUser",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"fonctionUser",$isFormulaireValide); ?>

                            <label for="mailUser">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Adresse e-mail :
                            </label>
                            <input type="email" name="mailUser" id="mailUser" required=""
                                   value="<?php tryToEcho("mailUser",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"mailUser",$isFormulaireValide); ?>

                            <label for="telUser">Téléphone :</label>
                            <input type="tel" name="telUser" id="telUser"
                                   value="<?php tryToEcho("telUser",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"telUser",$isFormulaireValide); ?>
                        </div>

                        <div id="chorus_pro">
                            <h3>Informations relatives à Chorus Pro : </h3>
                            <label for="siretCollectivite">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                SIRET :
                            </label>
                            <input type="number" name="siretCollectivite" id="siretCollectivite" required=""
                                   value="<?php tryToEcho("siretCollectivite",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"siret",$isFormulaireValide); ?>

                            <label for="numeroEngagementCollectivite">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                N° d'engagement :
                            </label>
                            <input type="text" name="numeroEngagementCollectivite" id="numeroEngagementCollectivite"
                                   required=""
                                   value="<?php tryToEcho("numeroEngagementCollectivite",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"numeroEngagement",$isFormulaireValide); ?>

                            <label for="codeCollectivite">
                                <abbr class="adullact-required" title="Champ obligatoire">*</abbr>
                                Code :
                            </label>
                            <input type="text" name="codeCollectivite" id="codeCollectivite" required=""
                                   value="<?php tryToEcho("codeCollectivite",$isFormulaireValide); ?>"><br>
                            <?php tryToShowError($errors,"code",$isFormulaireValide); ?>
                        </div>

                        <input type="submit" id="boutonEnvoyer" value="Envoyer"/>
                    </form>

                    <script src="<?php echo $JSPath ?>"></script>

                    <script>
                        prepareDocument();
                    </script>
                </main>
                <?php
            }
            ?>
        </div>
    </div>

<?php get_footer(); ?>