<?php

/**
 * liste des descriptions, que l'on peut définir pour ajouter des infos complémentaires aux différents types de
 * collectivités
 */
$descriptions = array(
    "Association" => "(Loi 1901)",
    "Département" => "",
    "EPCI Mutualisant" => "",
    "EPCI à fiscalité propre" => "",
    "Etablissement Public" => "(OPH, EPCC, EPCI sans fiscalité propre, syndicat)",
    "Individu" => "",
    "Région" => "",
    "Ville" => "",
);

/**
 * les variables suivantes vont servir à la bonne génération du formulaire d'adhésion et au traitement d'erreurs lors de son envoi
 */
$theme_path_for_browser = get_stylesheet_directory_uri();
$theme_path_for_server = get_stylesheet_directory();
$formulaire_adhesion_path = $theme_path_for_browser . "/formulaire-adhesion/";

$grillePath = $formulaire_adhesion_path . 'adhesion-tarif-par-nature-et-taille.csv';
$traitementPath = 'traitement-adhesion.php';
$periodSelectionPath = "period-selection.php";
$JSPath = $formulaire_adhesion_path . 'adhesion-form.js';

define("DEBUG_FORM",false);
$resultat = false;
$isFormulaireValide = false;

$errors = array(
    //Liste des éléments pouvant avoir une erreur
    "first selection" => "",
    "second Selection" => "",
    "EPCI com" => "",
    "EPCI hab" => "",
    "nom" => "",
    "adresse" => "",
    "CP" => "",
    "ville" => "",
    "tel" => "",
    "site" => "",
    "periode" => "",
    "siret" => "",
    "numeroEngagement" => "",
    "code" => "",
    "nomUser" => "",
    "prenomUser" => "",
    "fonctionUser" => "",
    "mailUser" => "",
    "telUser" => "",
);

$numberOfErrors = 0;
if (DEBUG_FORM) {
    var_dump($_POST);
}
if (isset($_POST)) {
    if (isset($_POST["firstSelection"])) {
        if (!isset($_POST["adullact_validation_checkbox"])) {
            //c'est bien un utilisateur car checkbox pas cochée
            require_once $traitementPath;
            if (DEBUG_FORM) {
                echo "<br> début du traitement <br>";
            }
            $_POST = array_map("htmlspecialchars", $_POST);
            $errors = traitement($_POST, $errors);

            if (DEBUG_FORM) {
                echo "<br> fin du traitement <br>";
            }
            foreach ($errors as $key => $value) {

                if (DEBUG_FORM) {
                    echo $key . " : " . $value . "<br>";
                }

                if (strlen($value) > 0) {
                    $numberOfErrors += 1;
                }
            }
            if (DEBUG_FORM) {
                echo "<br> nombre d'erreurs : " . $numberOfErrors . " <br>";
                var_dump($errors);
            }
            if ($numberOfErrors == 0) {
                sendAdhesionMail($_POST,ELEMENTS_OF_POST_TO_TRASH,REPLACEMENTS_OF_POST);
                debug_traitement_adhesion("mail envoyé");
                $isFormulaireValide = true;
                //header("Location:");
            }

        } else {
            header("Location:/");
        }
    }
}

require_once $periodSelectionPath;

$fichier = fopen($grillePath, 'r');
$ligne = fgetcsv($fichier, 1024, ";");

//on effectue de nouveau cette instruction pour sauter les titres du tableau
$ligne = fgetcsv($fichier, 1024, ";");

//------------------------Variables-------------------

$listeGrilleEntiere = [];        //contiendra l'intégralité des données dans adhesion-tarif-par-nature-et-taille.csv
$textsSelectMenu = [];           //contiendra les textes du premier select option gérés automatiquement
$textsSelectMenuExtended = [];   //même rôle qu'au dessus mais en ajoutant les options gérées automatiquement + manuellement

$classSelectMenu = [];           //contiendra les classes des option menu gérés automatiquement
$classSelectMenuExtended = [];   //contiendra les classes des option menu gérés automatiquement + manuellement
$descriptionFirstSelectMenu = [];//contiendra la description des différentes options du premier select.

while ($ligne) {
    //boucle qui va récupérer tout le contenu du fichier pour le stocker dans listeGrilleEntiere
    $listeGrilleEntiere[] = ($ligne);
    $ligne = fgetcsv($fichier, 1024, ";");
}

fclose($fichier);

//on crée la première liste qui gère les textes de la première liste, avec accents et espaces
$textsSelectMenu = createFirstSelectionList($listeGrilleEntiere);

//on ajoute "association", epci mutualisant et "individu" qui possèdent un traitement différent
$textsSelectMenuExtended = $textsSelectMenu;
$textsSelectMenuExtended[] = ("Association");
$textsSelectMenuExtended[] = ("Individu");
$textsSelectMenuExtended[] = ("EPCI Mutualisant");

//on crée la liste pour le 2e select qui contient le nom des classes gérés automatiquement
$classSelectMenu = createSizeSelectList($listeGrilleEntiere);

//classes gérées automatiquement + manuellement
$classSelectMenuExtended = $classSelectMenu;

$classSelectMenuExtended[] = ("Association");
$classSelectMenuExtended[] = ("Individu");
$classSelectMenuExtended[] = ("EPCIMutualisant");

//on trie par ordre alphabétique
sort($textsSelectMenu);
sort($textsSelectMenuExtended);
sort($classSelectMenu);
sort($classSelectMenuExtended);

function createFirstSelectionList($liste)
{
    //fonction qui crée la première liste de sélection sans modifier les textes
    $listeTempo = [];
    for ($a = 0; $a < count($liste); $a++) {
        $mot = $liste[$a][0];
        if (!isWordIn($mot, $listeTempo)) {
            $listeTempo[] = $mot;
        }
    }
    return $listeTempo;
}

/**
 * On "nettoie" $variable pour pouvoir l'utiliser en tant que nom de classe HTML
 * @param string $variable
 * @return string
 */
function createNameClass($variable)
{
    $chars = array(
        //tous les caractères susceptibles d'être remplacés
        "é" => "e",
        "è" => "e",
        "ê" => "e",
        "ë" => "e",
        "à" => "a",
        "â" => "a",
        "ù" => "u",
        "û" => "u",
        "î" => "i",
        "ï" => "i",
        "ç" => "c",

        "É" => "e",
        "È" => "e",
        "Ê" => "e",
        "Ë" => "e",
        "À" => "a",
        "Â" => "a",
        "Ù" => "u",
        "Û" => "u",
        "Î" => "i",
        "Ï" => "i",
        "Ç" => "c",

        " " => "_"
    );
    $variable = strtr($variable, $chars);
    return $variable;
}

/**
 * S'occupe de créer la liste des noms de classes HTML qui vont être utilisées dans le second select menu du formulaire.
 * @param array $liste : la liste des textes affichés à l'utilisateur
 * @return array $resultList : la liste des textes modifiés de façon à les utiliser pour des classes HTML.
 */
function createSizeSelectList($liste)
{
    //fonction qui crée la liste des classes qui sert pour le 2e select
    $resultList = [];
    for ($a = 0; $a < count($liste); $a++) {
        $mot = createNameClass($liste[$a][0]);
        if (!isWordIn($mot, $resultList)) {
            $resultList[] = $mot;
        }
    }
    return $resultList;
}

/**
 * Génère les différentes périodes d'adhésion possibles. Elle crée les <option> utilisées dans le <select> associé.
 * @param array $liste : la grille du fichier csv
 */
function generateCotisations($liste)
{
    $classInTreatment = "";
    $counter = 1;

    for ($a = 0; $a < count($liste); $a++) {
        //boucle qui génère les montants des cotisations

        //on récupère la ligne
        $actualLine = $liste[$a];
        $actualClass = createNameClass($actualLine[0]);

        //on vérifie si on change de classe ou non pour remettre le compteur à 1
        if ($classInTreatment != $actualClass) {
            $counter = 1;
            $classInTreatment = $actualClass;
        }
        //la tranche pour la cotisation
        $id = "PCOT-" . $classInTreatment . "-T" . $counter;
        ?>
        <p id="<?php echo $id ?>" class="PCOT">Montant de la cotisation : <?php echo $actualLine[2] ?>
            €</p>
        <?php
        $counter += 1;
    }
}

/**
 * On génère les label pour les différentes cotisations en fonction de la taille et du type de la collectivité.
 * Javascript va se charger de n'afficher que la bonne au bon moment.
 * @param array $liste : grille du fichier csv
 */
function generateLabelCotisations($liste)
{
    for ($i = 0; $i < count($liste); $i++) {
        if (isset($liste[$i][3])) {
            $id = "TYPENBR-" . createNameClass($liste[$i][0]);
            echo "<label id=\"" . $id . "\" for=\"secondSelection\"> 
            <abbr class=\"adullact-required\" title=\"Champ obligatoire\">*</abbr>
            Veuillez sélectionner le nombre " . $liste[$i][3] . " :</label> \n";
        }
    }
}

/**
 * On génère les options du second <select> : les différentes tailles de collectivités en fonction du type de collectivité.
 * Javascript va se charger d'afficher uniquement les bonnes au bon moment.
 * @param array $liste : grille du fichier csv
 */
function generateSecondSelection($liste)
{
    $classInTreatment = "";
    $counter = 1;

    for ($a = 0; $a < count($liste); $a++) {
        //boucle qui génère les options du second select

        //on récupère la ligne
        $actualLine = $liste[$a];
        $actualClass = createNameClass($actualLine[0]);

        //on vérifie si on change de classe ou non pour remettre le compteur à 1
        if ($classInTreatment != $actualClass) {

            $counter = 1;
            $classInTreatment = $actualClass;

            ?>
            <option class="<?php echo $classInTreatment ?>" value="" data-type="optionSelect2">---
            </option>
            <?php
        }
        //la tranche pour la cotisation
        $tranch = $classInTreatment . "-T" . $counter;

        ?>
        <option class="<?php echo $classInTreatment ?>" id="<?php echo $tranch ?>" data-type="optionSelect2"
                value="<?php echo str_replace("<", "&lt;", $actualLine[1]); ?>"
        ><?php echo str_replace("<", "&lt;", $actualLine[1]); ?></option>
        <?php
        $counter += 1;
    }
}

/**
 * Est ce que $word est contenu dans $list ?
 * @param string $word
 * @param array $list
 * @return bool
 */
function isWordIn($word, $list)
{
    for ($a = 0; $a < count($list); $a++) {
        if ($word == $list[$a]) {
            return true;
        }
    }
    return false;
}

/**
 * On va essayer d'afficher la précédente valeur entrée lorsque une erreur est survenue.
 * @param string $var : la clé recherchée dans $_POST
 * @param $isValidForm : si le formulaire n'est pas validé, alors il y a eu une erreur, on peut donc tenter de l'afficher
 */
function tryToEcho($var,$isValidForm)
{
    if (!$isValidForm) {
        if (isset($_POST[$var])) {
            echo $_POST[$var];
        }
    }
}

/**
 * Si une mauvaise erreur a été entrée par l'utilisateur, il faut remettre le <select> comme il était avant l'envoi..
 * @param $key
 * @param $value
 */
function tryToSetInputSelect($key, $value)
{
    if (isset($_POST[$key])) {

        if ($_POST[$key] == $value) {
            echo "selected";
        }
    }
}

/**
 * On essaye d'afficher une erreur s'il y en a une.
 * @param array $errors_array : l'array des erreurs
 * @param string $var : la clé recherchée dans le tableau d'erreurs.
 * @param $isValidForm : le formulaire est validé ou non ?
 */
function tryToShowError($errors_array,$var,$isValidForm)
{
    if (!$isValidForm) {
        if (strlen($errors_array[$var]) > 0) {
            echo "<p class='adullact-error'> " . $errors_array[$var] . "</p>";
        } else {
            if (DEBUG_FORM) {
                echo "pas d'erreur : " . $errors_array[$var];
            }
        }
    }
}