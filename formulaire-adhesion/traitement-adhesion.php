<?php

/**
 * Toutes ces variables ne sont que des textes modifiables par le développeur/mainteneur. Ils ne sont là que pour afficher des
 * informations.
 */
//variables pour le mail
define("SUBJECT_ADULLACT", "Nouvelle demande de renouvellement d'adhésion");
define("SUBJECT_ERROR_MAIL", "Une erreur est survenue lors d'un envoi de mail récupéré sur le formulaire d'adhésion.");
define("MESSAGE_ERROR_MAIL", "Lorsqu'un utilisateur a validé le formulaire d'adhésion, le mail de confirmation n'a pas été envoyé à cet utilisateur. Une erreur est survenue, veuillez consulter les logs pour en savoir plus.");
define("SUBJECT_USER", "Confirmation de demande de renouvellement d'adhésion");

$bonjourUser = "Bonjour,";
$textUser = "Nous avons bien reçu votre demande de renouvellement d'adhésion. Nous vous recontacterons sous peu.";
$byeUser = "A très bientôt !";
$endUser = "L'équipe Adullact.";
define("TEXT_MAIL_USER", $bonjourUser . "<br>" . $textUser . "<br>" . $byeUser . "<br>" . $endUser);

define("FILTER_FIRST_NAME", "/^([a-zA-ZÀ-ÖØ-öø-ÿœŒ' -]+)$/");
define("FILTER_ADDRESS", "/^([a-zA-Z0-9À-ÖØ-öø-ÿœŒ' -]+)$/");

//nombres maximum
define("MAX_ADDRESS_CHARACTERS", 255);
define("MAX_NAME_CHARACTERS", 255);
define("MAX_CITY_CHARACTERS", 255);
define("MAX_FONCTION_CHARACTERS", 255);

//Messages d'erreur
define("ERR_TOO_LONG_ADDRESS", "L'adresse que vous avez entrée est trop longue. Maximum : " . MAX_ADDRESS_CHARACTERS);
define("ERR_TOO_LONG_NAME", "Le nom que vous avez entré est trop long. Maximum : " . MAX_NAME_CHARACTERS);
define("ERR_TOO_LONG_NAME_USER", "Le prénom que vous avez entré est trop long. Maximum : " . MAX_NAME_CHARACTERS);
define("ERR_TOO_LONG_USER_FUNCTION", "La fonction que vous avez entrée est trop longue. Maximum : " . MAX_FONCTION_CHARACTERS);
define("ERR_TOO_LONG_CITY", "Le nom de la ville est trop long. Maximum : " . MAX_CITY_CHARACTERS);

define("ERR_POSTAL_CODE", "Un code postal est composé de 5 chiffres.");
define("ERR_WRONG_CHARACTER", "Un ou plusieurs caractères incorrects ont été entrés.");
define("ERR_MAIL", "L'adresse E-mail est invalide.");
define("ERR_TEL", "Un numéro de téléphone se compose de 10 chiffres et commence par un zéro.");
define("ERR_URL", "L'URL est incorrecte.");
define("ERR_PERIOD_SELECTION", "Veuillez sélectionner une période d'adhésion.");
define("ERR_IN_SIRET", "Un numéro de SIRET est composé de 14 chiffres.");
define("ERR_INVALID_SIRET", "Le numéro de SIRET est invalide.");
define("ERR_POSITIVE_NUMBER_REQUIRED", "Vous devez entrer un nombre positif.");
define("ERR_NUMBER_REQUIRED", "Vous devez entrer un nombre.");

define("ERR_FIELD_REQUIRED", "Vous devez compléter ce champ.");
define("ERR_SIZE_REQUIRED", "Vous devez sélectionner la taille de votre collectivité.");

define("DEBUG_TRAITEMENT_ADHESION", false);
define("ELEMENTS_OF_POST_TO_TRASH", ["ancienSelectType", "ancienSelectSize", "ancienSelectPeriod"]);

define("REPLACEMENTS_OF_POST", array(
    "firstSelection" => "Type de collectivité",
    "secondSelection" => "Taille de la collectivité",
    "nombreHabitantsEPCI" => "Nombre d'habitants EPCI",
    "nombreCommunesMembresEPCI" => "Nombre de communes membres EPCI",
    "nomCollectivite" => "Nom de la collectivité",
    "adresseCollectivite" => "Adresse de la collectivité",
    "codePostalCollectivite" => "Code postal",
    "villeCollectivite" => "Ville",
    "telCollectivite" => "Téléphone",
    "siteCollectivite" => "Site internet",
    "periodeAdhesionCollectivite" => "Période d'adhésion",
    "siretCollectivite" => "N° de SIRET",
    "numeroEngagementCollectivite" => "Numéro d'engagement",
    "codeCollectivite" => "Code service",
    "nomUser" => "Nom du représentant",
    "prenomUser" => "Prénom du représentant",
    "fonctionUser" => "Fonction",
    "mailUser" => "Email du représentant",
    "telUser" => "Téléphone du représentant",
));

/**
 * Fonction qui utilise l'algorithme de Luhn pour vérifier si le numéro de siret est OK.
 * @param string $numero
 * @return bool
 */
function luhn($numero)
{
    $longueur = 14;
    $tableauChiffresNumero = array();
    if (preg_match("#[0-9]{" . $longueur . "}#i", $numero)) {
        for ($i = 0; $i < $longueur; $i++) {
            $tableauChiffresNumero[$i] = substr($numero, $i, 1);
        }
        $luhn = 0; // clef de luhn à tester
        for ($i = 0; $i < $longueur; $i++) {
            if ($i % 2 == 0) { // si le rang est pair (0,2,4 etc.)
                if (($tableauChiffresNumero[$i] * 2) > 9) {
                    $tableauChiffresNumero[$i] = ($tableauChiffresNumero[$i] * 2) - 9;

                } else {
                    $tableauChiffresNumero[$i] = $tableauChiffresNumero[$i] * 2;
                }
            }
            $luhn = $luhn + $tableauChiffresNumero[$i];
        }
        if ($luhn % 10 == 0) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * fonction utilisée lors du débogage. si on active le débogage via la variable $debug_traitement_adhesion, alors de
 * nombreux messages informatifs seront affichés.
 * @param string $message
 */
function debug_traitement_adhesion($message)
{
    if (DEBUG_TRAITEMENT_ADHESION) {
        echo "<p> $message </p>";
    }
}

/**
 * fonction utile pour l'envoi de mail grâce au $_POST
 * on étudie le $_POST et on transfère les lignes utiles à $tableauFinal, on ne garde pas le reste.
 * @param array $tab : $_POST
 * @param $trash : les éléments du $_POST qui ne vont pas être pris en compte dans l'écriture du mail
 * @param $replacements : les textes à remplacer
 */
function sendAdhesionMail($tab, $trash, $replacements)
{
    debug_traitement_adhesion("on se prépare à envoyer le mail");
    $tableauFinal = array();

    //on prépare le tableau des textes
    foreach ($tab as $key => $value) {
        if (!isWordIn($key, $trash)) {
            if ($value != "" && $value != "none") {

                //transformation du nom de la classe -> texte donné dans $replacements
                $key = $replacements[$key];
                //on évite faille XSS
                $value = htmlspecialchars($value);
                $tableauFinal[$key] = $value;
            }
        }
    }
    $emailUser = trim($tab["mailUser"]);

    $header = "Content-type: text/html; charset=utf-8\r\n";
    $header .= "Content-Transfer-Encoding: 8bit\r\n";

    $logMessage = "\n";
    $messageAdullact = "Nouvelle requête pour le formulaire d'adhésion : <br><table>";
    //on prépare le mail à destination de l'adullact ainsi que les logs
    foreach ($tableauFinal as $key => $value) {
        $key = str_replace("\n", "", $key);
        $value = str_replace("\n", "", $value);
        $messageAdullact = $messageAdullact . "<tr><td>" . $key . " : </td> <td>" . $value . "</td>" . "</tr>";
        $logMessage = $logMessage . $key . " : " . $value . "\n";
    }

    //on tente d'envoyer le mail à l'utilisateur
    if (wp_mail($emailUser, html_entity_decode(SUBJECT_USER, ENT_QUOTES, 'UTF-8'), TEXT_MAIL_USER, $header)) {
        writeInLog("Nouvelle demande", $logMessage, MEMBERSHIP_LOGS_DIRECTORY);
        //si ça fonctionne, on envoie un mail positif à l'Adullact.
        if (!wp_mail(MAIL_CIBLE_ADULLACT, html_entity_decode(SUBJECT_ADULLACT, ENT_QUOTES, 'UTF-8'), $messageAdullact, $header)) {
            //on envoie le mail à l'adullact mais si ça échoue, on écrit dans les logs.
            $entire_error = "Adresse mail : " . MAIL_CIBLE_ADULLACT . "\n";
            $entire_error .= "Headers du mail : " . $header . "\n";
            $entire_error .= "Sujet mail : " . SUBJECT_ADULLACT . "\n";
            $entire_error .= "Message mail : " . $messageAdullact . "\n";

            writeInLog("Erreur lors de l'envoi de mail à l'Adullact", $entire_error, MEMBERSHIP_LOGS_DIRECTORY);
        }
    } else {
        //l'envoi à l'utilisateur n'a visiblement pas fonctionné. On tente donc d'envoyer un mail négatif à l'Adullact et on l'écrit dans les logs.
        if (!wp_mail(MAIL_CIBLE_ADULLACT, html_entity_decode(SUBJECT_ERROR_MAIL, ENT_QUOTES, 'UTF-8'), MESSAGE_ERROR_MAIL, $header)) {
            //si l'envoi de mail à l'Adullact ne fonctionne pas non plus, on signale les deux erreurs.
            writeInLog("Erreur lors de l'envoi de mail à" . $emailUser, "Header : \n" . $header . "\nSujet : " . SUBJECT_USER . "\nInfos :" . $logMessage, MEMBERSHIP_LOGS_DIRECTORY);
            writeInLog("Erreur lors de l'envoi de mail à L'Adullact", "Header : \n" . $header . "\nSujet : " . SUBJECT_ERROR_MAIL . "\nInfos :" . MESSAGE_ERROR_MAIL, MEMBERSHIP_LOGS_DIRECTORY);
        }
    }
}

/**
 * Utile pour écrire un quelconque message dans les logs
 * @param string $intitule
 * @param string $text
 * @param string $filePath : chemin d'accès au fichier de log
 */
function writeInLog($intitule, $text, $filePath)
{
    //fonction qui écrit dans les logs.
    date_default_timezone_set('Europe/Paris');

    $date = date("d-m-Y");
    $hour = date("H:i:s");
    $beforeMessage = "\n--------------\nLe " . $date . ", à " . $hour . ", \n" . $intitule . " : \n";
    $finalMessage = $beforeMessage . $text;
    $fichier = fopen($filePath, "a");

    if (fwrite($fichier, $finalMessage)) {
        debug_traitement_adhesion("fichier écrit");

    } else {
        debug_traitement_adhesion("fichier pas écrit");
        debug_traitement_adhesion("chemin : " . $filePath);
    }
    fclose($fichier);
}

//-----------------------Toutes les fonctions de vérification de données------------------------

/**
 * On vérifie la valeur du premier <select>
 * @param array $tab ($_POST)
 * @return bool
 */
function verifFirstSelection($tab)
{
    if (isset($tab["firstSelection"])) {
        //première sélection présente
        if ($tab["firstSelection"] != "") {
            //l'utilisateur a sélectionné quelque chose. mais quoi ?
            return true;

        } else {
            //sélection égale à "none" ou ""
            return false;
        }
    } else {
        //première sélection absente du formulaire
        return false;
    }
}

/**
 * On vérifie la valeur du second <select>
 * @param array $tab
 * @param array $errors
 * @return mixed
 */
function verifSecondSelection($tab, $errors)
{
    if (isset($tab["secondSelection"])) {
        if ($tab["secondSelection"] == "" || $tab["secondSelection"] == "none") {
            $errors["second Selection"] = ERR_SIZE_REQUIRED;
        }
    } else {
        $errors["second Selection"] = ERR_SIZE_REQUIRED;
    }
    return $errors;
}

/**
 * On vérifie les deux nombres donnés pour calculer la cotisation de l'EPCI
 * @param array $tab
 * @param array $errors
 * @return array mixed
 */
function verifEPCI($tab, $errors)
{
    if (isset($tab["nombreHabitantsEPCI"])) {
        if (isset($tab["nombreCommunesMembresEPCI"])) {
            $nombreHab = $tab["nombreHabitantsEPCI"];
            $nombreCom = $tab["nombreCommunesMembresEPCI"];

            if (is_numeric($nombreHab)) {
                if (is_numeric($nombreCom)) {
                    if ($nombreHab > 0) {
                        if ($nombreCom > 0) {

                        } else {
                            $errors["EPCI com"] = ERR_POSITIVE_NUMBER_REQUIRED;
                        }
                    } else {
                        $errors["EPCI hab"] = ERR_POSITIVE_NUMBER_REQUIRED;
                    }
                } else {
                    $errors["EPCI com"] = ERR_NUMBER_REQUIRED;
                }
            } else {
                $errors["EPCI hab"] = ERR_NUMBER_REQUIRED;
            }
        } else {
            $errors["EPCI hab"] = ERR_NUMBER_REQUIRED;
        }
    } else {
        $errors["EPCI hab"] = ERR_NUMBER_REQUIRED;
    }
    return $errors;
}

/**
 * On vérifie le nom de la collectivité
 * @param array $tab
 * @return string
 */
function verifNom($tab)
{
    if (isset($tab["nomCollectivite"])) {
        $nom = trim($tab["nomCollectivite"]);
        if (strlen($nom) > 0) {
            if (strlen($nom) < MAX_NAME_CHARACTERS) {
                if (preg_match(FILTER_FIRST_NAME, $nom)) {
                    return "";
                } else {
                    return ERR_WRONG_CHARACTER;
                }
            } else {
                return ERR_TOO_LONG_NAME;
            }
        } else {
            return ERR_FIELD_REQUIRED;
        }
    }
    return ERR_FIELD_REQUIRED;
}

/**
 * Vérification de l'adresse
 * @param array $tab
 * @return string
 */
function verifAdresse($tab)
{
    if (isset($tab["adresseCollectivite"])) {
        $adresse = trim($tab["adresseCollectivite"]);
        if (strlen($adresse) > 0) {
            if (strlen($adresse) < MAX_ADDRESS_CHARACTERS) {
                if (preg_match(FILTER_ADDRESS, $adresse)) {
                    return "";
                } else {
                    return ERR_WRONG_CHARACTER;
                }
            } else {
                return ERR_TOO_LONG_ADDRESS;
            }
        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**
 * Vérification du code postal
 * @param array $tab
 * @return string
 */
function verifCodePostal($tab)
{
    if (isset($tab["codePostalCollectivite"])) {
        $CP = trim($tab["codePostalCollectivite"]);
        debug_traitement_adhesion("code postal entré : " . $tab["codePostalCollectivite"]);
        if (strlen($CP) > 0) {
            if (strlen($CP) == 5 && is_numeric($CP)) {
                return "";
            } else {
                return ERR_POSTAL_CODE;
            }
        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**
 * Vérification de la ville
 * @param array $tab
 * @return string
 */
function verifVille($tab)
{
    //vérifie si la ville est correcte
    if (isset($tab["villeCollectivite"])) {
        $ville = trim($tab["villeCollectivite"]);
        if (strlen($ville) > 0) {
            if (strlen($ville) < MAX_CITY_CHARACTERS) {
                if (preg_match(FILTER_FIRST_NAME, $ville)) {
                    return "";
                } else {
                    return ERR_WRONG_CHARACTER;
                }
            } else {
                return ERR_TOO_LONG_CITY;
            }
        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**
 * Vérification du mail via une fonction implémentée dans php
 * @param array $tab
 * @param string $key
 * @return string
 */
function verifMail($tab, $key)
{
    if (isset($tab[$key])) {
        $mail = trim($tab[$key]);
        if (strlen($mail) > 0) {
            if (filter_var($mail, FILTER_VALIDATE_EMAIL) == true) {
                return "";
            } else {
                return ERR_MAIL;
            }
        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**
 * Vérification du nom de famille
 * @param array $tab
 * @return string
 */
function verifNomUser($tab)
{
    if (isset($tab["nomUser"])) {
        $nom = trim($tab["nomUser"]);
        if (strlen($nom) > 0) {
            if (strlen($nom) <= MAX_NAME_CHARACTERS) {
                if (preg_match(FILTER_FIRST_NAME, $nom)) {
                    return "";
                } else {
                    return ERR_WRONG_CHARACTER;
                }
            } else {
                return ERR_TOO_LONG_NAME;
            }
        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**Vérification du prénom de l'utilisateur
 * @param array $tab
 * @return string
 */
function verifPrenomUser($tab)
{
    if (isset($tab["prenomUser"])) {
        $prenom = trim($tab["prenomUser"]);
        if (strlen($prenom) > 0) {
            if (strlen($prenom) <= MAX_NAME_CHARACTERS) {
                if (preg_match(FILTER_FIRST_NAME, $prenom)) {
                    return "";
                } else {
                    return ERR_WRONG_CHARACTER;
                }
            } else {
                return ERR_TOO_LONG_NAME_USER;
            }
        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**
 * Vérification de la fonction de l'utilisateur dans la collectivité
 * @param array $tab
 * @return string
 */
function verifFonctionUser($tab)
{
    if (isset($tab["fonctionUser"])) {
        $fonction = trim($tab["fonctionUser"]);
        if (strlen($fonction) > 0) {
            if (strlen($fonction) <= MAX_FONCTION_CHARACTERS) {
                if (preg_match(FILTER_FIRST_NAME, $fonction)) {
                    return "";
                } else {
                    return ERR_WRONG_CHARACTER;
                }
            } else {
                return ERR_TOO_LONG_USER_FUNCTION;
            }
        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**
 * Vérification du numéro de téléphone
 * @param array $tab
 * @param string $key
 * @return string
 */
function verifTel($tab, $key)
{
    //vérifie le numéro de téléphone. il n'est pas obligatoire, on ne retourne pas d'erreur si vide.
    if (isset($tab[$key])) {
        $tel = trim($tab[$key]);
        if (strlen($tel) > 0) {

            //vérifie si le téléphone a été correctement entré
            if ((strlen($tel) == 10 && is_numeric($tel)) && $tel[0] == "0") {
                return "";
            } else {
                return ERR_TEL;
            }
        } else {
            return "";
        }
    } else {
        return "";
    }
}

/**
 * Vérification de l'url du site via fonction implémentée dans php
 * @param array $tab
 * @return string
 */
function verifUrl($tab)
{
    //vérifie l'Url du site internet grâce à un filter var. pas obligatoire, ne retourne pas d'erreurs si vide.

    if (isset($tab["siteCollectivite"])) {
        $url = trim($tab["siteCollectivite"]);
        if (strlen($url) > 0) {
            if (filter_var($url, FILTER_VALIDATE_URL) == true) {
                return "";
            } else {
                return ERR_URL;
            }
        } else {
            return "";
        }
    } else {
        return "";
    }
}

/**
 * Vérification de la période d'adhésion
 * @param array $tab
 * @return string
 */
function verifPeriodeAdhesion($tab)
{
    //vérifie que la période d'adhésion est OK
    if (isset($tab["periodeAdhesionCollectivite"])) {
        if (strlen($tab["periodeAdhesionCollectivite"]) > 0) {
            return "";
        } else {
            return ERR_PERIOD_SELECTION;
        }
    } else {
        return ERR_PERIOD_SELECTION;
    }
}

/**
 * Vérification du numéro de siret, avec la fonction Luhn()
 * @param array $tab
 * @return string
 */
function verifSiret($tab)
{
    debug_traitement_adhesion("vérification siret");
    if (isset($tab["siretCollectivite"])) {
        $siret = trim($tab["siretCollectivite"]);
        if (strlen($siret) > 0) {
            if (strlen($siret) == 14 && is_numeric($siret)) {
                if (luhn($siret)) {
                    return "";
                }
                return ERR_INVALID_SIRET;
            } else {
                return ERR_IN_SIRET;
            }
        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**
 * Vérification du numéro d'engagement
 * @param array $tab
 * @return string
 */
function verifNumeroEngagement($tab)
{
    //vérifie si le numéro engagement a été correctement entré
    if (isset($tab["numeroEngagementCollectivite"])) {
        $numero = trim($tab["numeroEngagementCollectivite"]);
        if (strlen($numero) > 0) {
            return "";

        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**
 * Vérification du code service
 * @param array $tab
 * @return string
 */
function verifCode($tab)
{
    //vérifie si le code service a été correctement entré
    if (isset($tab["codeCollectivite"])) {

        $code = trim($tab["codeCollectivite"]);
        if (strlen($code) > 0) {
            return "";
        } else {
            return ERR_FIELD_REQUIRED;
        }
    } else {
        return ERR_FIELD_REQUIRED;
    }
}

/**
 * vérifie tous les champs input
 * on vérifie uniquement celles utiles, en fonction de ce que l'utilisateur a entré
 * @param array $tab
 * @param array $errors
 * @return mixed
 */
function traitementInformations($tab, $errors)
{
    $errors["nom"] = verifNom($tab);
    $errors["adresse"] = verifAdresse($tab);
    $errors["CP"] = verifCodePostal($tab);
    $errors["ville"] = verifVille($tab);
    $errors["tel"] = verifTel($tab, "telCollectivite");
    $errors["site"] = verifUrl($tab);
    $errors["periode"] = verifPeriodeAdhesion($tab);

    $errors["nomUser"] = verifNomUser($tab);
    $errors["prenomUser"] = verifPrenomUser($tab);
    $errors["fonctionUser"] = verifFonctionUser($tab);
    $errors["mailUser"] = verifMail($tab, "mailUser");
    $errors["telUser"] = verifTel($tab, "telUser");

    if ($tab["firstSelection"] != "Association") {
        debug_traitement_adhesion("tab[firstselection] = " . $tab["firstSelection"]);
        $errors["siret"] = verifSiret($tab);
        $errors["numeroEngagement"] = verifNumeroEngagement($tab);
        $errors["code"] = verifCode($tab);
    }
    return $errors;
}

/**
 * traitement principal, liste des actions à effectuer dans un certain ordre
 * @param array $tab
 * @param array $errors
 * @return array|mixed
 */
function traitement($tab, $errors)
{
    debug_traitement_adhesion("début traitement");

    //le résultat du premier select
    if (verifFirstSelection($tab)) {
        debug_traitement_adhesion("first select ok");

        if ($tab["firstSelection"] == "Association") {
            //il n'y a besoin que de traiter les dernières infos
            $errors = traitementInformations($tab, $errors);
        } elseif ($tab["firstSelection"] == "EPCIMutualisant") {
            $errors = verifEPCI($tab, $errors);
            $errors = traitementInformations($tab, $errors);
        } else {
            //taille
            $errors = verifSecondSelection($tab, $errors);
            $errors = traitementInformations($tab, $errors);
        }
    } else {
        $errors["first selection"] = "Veuillez préciser le type de collectivité.";
    }
    if (DEBUG_TRAITEMENT_ADHESION) {
        echo "errors : <br>";
        var_dump($errors);
    }
    return $errors;
}