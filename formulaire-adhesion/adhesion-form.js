//liste des blocs qui peuvent être affichés / cachés
const blocks = ["link", "paraSecondSelection", "cotisation", "informationsCollectivite", "formulaireEPCIMutualisant", "boutonEnvoyer", "chorus_pro"];
const maxCotisation = 12000;

/**
 * Récupère tous les éléments en fonction de leur attribut "attribute"
 * @param attribute : string
 * @returns {[]}
 */
function getElementsByAttribute(attribute) {
    let list = document.getElementsByTagName('*');
    let result = [];

    for (let i = 0; i < list.length; i++) {
        if (list[i].getAttribute(attribute)) {
            result.push(list[i]);
        }
    }
    return result;
}

/**
 * fonction qui affiche / cache les formulaires / informations en fonction des données entrées dans les formulaires
 * @param display : array
 */
function displayMask(display) {
    for (let h = 0; h < blocks.length; h++) {
        if (isWordIn(blocks[h], display)) {
            document.getElementById(blocks[h]).style.display = "block";
        } else {
            document.getElementById(blocks[h]).style.display = "none";
        }
    }
}

/**
 * sert à afficher / cacher les options du 2e <select>, et modifier le label de cotisation associé
 * @param display : string
 */
function displayMaskOptions(display) {
    let toutesLesOptions = getElementsByAttribute("data-type");

    for (let j = 0; j < toutesLesOptions.length; j++) {
        //boucle for qui va parcourir la liste des classes des options disponibles

        if (display === toutesLesOptions[j].getAttribute("class")) {
            //si l'element en cours est celui de la classe recherchée

            if (toutesLesOptions[j].getAttribute("value") === "") {
                toutesLesOptions[j].selected = true;
            }
            toutesLesOptions[j].hidden = false;
            //on affiche le label
            document.getElementById("TYPENBR-" + toutesLesOptions[j].getAttribute("class")).style.display = "block";

        } else {

            toutesLesOptions[j].hidden = true;
            //On cache le label
            document.getElementById("TYPENBR-" + toutesLesOptions[j].getAttribute("class")).style.display = "none";
        }
    }
}

/**
 * fonction effectuée lorsque le premier select est validé
 */
function firstAction() {
    //on recup ce qui a été cliqué
    let e = document.getElementById("firstSelect");
    let strUser = e.options[e.selectedIndex].value;
    //on reagit en fonction

    //si il a cliqué sur individu, redirigé vers helloasso
    if (strUser === "Individu") {
        linkRedirection("https://www.helloasso.com/associations/adullact/adhesions/adhesion-individuelle-a-l-adullact", "Adhérer en tant qu'individu sur HelloAsso");
        displayMask(["link"]);
    }

    //si il a cliqué sur association
    else if (strUser === "Association") {
        setCotisation("Association");
        displayMask(["cotisation", "informationsCollectivite", "boutonEnvoyer"]);
        setSizeRequired(false);
        setEPCIRequired(false);
        setChorusProRequired(false);

    } else if (strUser === "EPCIMutualisant") {

        //formulaire contenant la formule spécifique à EPCI mutualisant
        displayMask(["formulaireEPCIMutualisant", "cotisation", "informationsCollectivite", "chorus_pro", "boutonEnvoyer"]);
        setSizeRequired(false);
        setEPCIRequired(true);
        setChorusProRequired(true);
        setCotisation("EPCIMutualisant", 1200);

    } else if (strUser === "") {
        //premier choix avec des tirets
        displayMask([""]);
    }

    //si il a cliqué sur autre chose : on cherche à connaitre la taille de la collectivité, on affiche le 2e select
    else {
        displayMaskOptions(strUser);
        setSizeRequired(true);
        displayMask(["paraSecondSelection", "cotisation", "informationsCollectivite", "chorus_pro", "boutonEnvoyer"]);
        setEPCIRequired(false);
        setChorusProRequired(true);
        setCotisation("EPCIMutualisant", 0);
    }
}

/**
 * Word est-il contenu dans list ?
 * @param word : string
 * @param list : array
 * @returns {boolean}
 */
function isWordIn(word, list) {
    //le word est il contenu dans list ?
    for (let a = 0; a < list.length; a++) {
        if (word === list[a]) {
            return true
        }
    }
    return false
}

/**
 * définit le lien de redirection et son titre
 * @param link : string
 * @param title : string
 */
function linkRedirection(link, title) {
    document.getElementById("linkRedirection").setAttribute("href", link);
    document.getElementById("linkRedirection").innerHTML = title;
}

/**
 * Première fonction appelée pour préparer le document, associer les boutons aux fonctions... etc
 */
function prepareDocument() {
    //prépare la page html en ajoutant les event listener
    displayMask([""]);
    document.getElementById("firstSelect").addEventListener("change", firstAction, false);
    document.getElementById("nombreHabitantsEPCI").addEventListener("change", traitementEPCIMutualisant, false);
    document.getElementById("nombreHabitantsEPCI").addEventListener("keyup", traitementEPCIMutualisant, false);
    document.getElementById("nombreCommunesMembresEPCI").addEventListener("change", traitementEPCIMutualisant, false);
    document.getElementById("nombreCommunesMembresEPCI").addEventListener("keyup", traitementEPCIMutualisant, false);
    document.getElementById("secondSelection").addEventListener("change", secondAction, false);
    setCotisation("");
    //si le formulaire a échoué, il y a probablement des zones à afficher en fonction de ce qui a été rempli
    let choiceType = document.getElementById("ancienSelectType").getAttribute("value");

    if (choiceType.length > 0) {
        let listOfFirstOptions = document.getElementsByClassName("optionFirstSelect");
        for (let a = 0; a < listOfFirstOptions.length; a++) {
            if (listOfFirstOptions[a].getAttribute("value") === choiceType) {
                listOfFirstOptions[a].selected = true;
                break;
            }
        }
        firstAction();
        if (choiceType === "EPCIMutualisant") {
            traitementEPCIMutualisant();
        } else {
            let choiceSize = document.getElementById("ancienSelectSize").getAttribute("value");
            if (choiceSize.length > 0 && choiceSize !== "none") {
                let listOfSizeOptions = document.getElementsByClassName(choiceType);

                for (let b = 0; b < listOfSizeOptions.length; b++) {
                    if (listOfSizeOptions[b].getAttribute("value") === choiceSize) {
                        listOfSizeOptions[b].selected = true;
                        break;
                    }
                }
                secondAction();
            }
        }
    }
    let choicePeriod = document.getElementById("ancienSelectPeriod").getAttribute("value");
    let listOfPeriodOptions = document.getElementsByClassName("optionPeriod");
    for (let c = 0; c < listOfPeriodOptions.length; c++) {
        if (listOfPeriodOptions[c].getAttribute("value") === choicePeriod) {
            listOfPeriodOptions[c].selected = true;
        }
    }
}

/**
 * fonction exécutée lorsque le 2e select est cliqué : on définit le montant de la cotisation en fonction du choix.
 */
function secondAction() {
    let e = document.getElementById("secondSelection");

    if (e.options[e.selectedIndex].value !== "") {
        //s'il a sélectionné autre chose que les tirets
        let selectedTranch = e.options[e.selectedIndex].getAttribute("id");

        setCotisation(selectedTranch);
        //displayMask("paraSecondSelection","cotisation","informationsCollectivite","chorus_pro","boutonEnvoyer");
    } else {
        setCotisation("EPCIMutualisant", 0);
    }
}

/**
 * définit la cotisation à afficher, l'option x sert uniquement pour le tarif EPCI mutualisant qui varie.
 * @param id : string
 * @param x : int : x va servir à définir un montant personnalisé à la cotisation. Sinon, on fait en fonction de l'id qui va chercher
 * la valeur correspondante pour la cotisation.
 */
function setCotisation(id, x = -1) {
    let paragraphCotList = document.getElementsByClassName("PCOT");
    let research = "PCOT-" + id;

    for (let b = 0; b < paragraphCotList.length; b++) {

        if (paragraphCotList[b].getAttribute("id") === research) {
            paragraphCotList[b].style.display = "block";
            if (x > -1) {
                document.getElementById("montantCotisation").innerHTML = x;
            }
        } else {
            paragraphCotList[b].style.display = "none";
        }
    }
}

/**
 * Le formulaire chorus pro est il requis ?
 * @param boolean : boolean
 */
function setChorusProRequired(boolean) {
    document.getElementById("siretCollectivite").required = boolean;
    document.getElementById("numeroEngagementCollectivite").required = boolean;
    document.getElementById("codeCollectivite").required = boolean;
}

/**
 * Le formulaire EPCI est il requis ?
 * @param boolean : boolean
 */
function setEPCIRequired(boolean) {
    //le formulaire EPCI est il requis ? cela varie selon les choix faits par l'utilisateur
    document.getElementById("nombreHabitantsEPCI").required = boolean;
    document.getElementById("nombreCommunesMembresEPCI").required = boolean;
}

/**
 * La taille de la collectivité (2e <select>) est elle requise ?
 * @param boolean : boolean
 */
function setSizeRequired(boolean) {
    document.getElementById("secondSelection").required = boolean;
}

/**
 * lorsque l'utilisateur valide les 2 nombres entrés pour l'EPCI, les traitements nécessaires sont ici
 */
function traitementEPCIMutualisant() {
    let nombreHabitants = document.getElementById("nombreHabitantsEPCI").value;
    let nombreCommunesMembres = document.getElementById("nombreCommunesMembresEPCI").value;

    if (nombreHabitants !== "" && parseInt(nombreHabitants) >= 0) {
        if (nombreCommunesMembres !== "" && parseInt(nombreCommunesMembres) >= 0) {

            let resultat = (0.035 * nombreHabitants) + (15 * nombreCommunesMembres) + 1200;

            //on arrondit à la centaine
            resultat = resultat / 100;
            resultat = Math.round(resultat);
            resultat = resultat * 100;
            //cette fonction arrondit le nombre en fonction du nombre après la virgule.
            //exemple :
            // Math.round(20,4999) donnera 20
            // Math.round(20,5) donnera 21

            if (resultat > maxCotisation) {
                //valeur fixée en haut du code
                resultat = maxCotisation;
            }

            setCotisation("EPCIMutualisant", resultat);
            displayMask(["cotisation", "informationsCollectivite", "formulaireEPCIMutualisant", "boutonEnvoyer", "chorus_pro"]);
        } else {
            setCotisation("EPCIMutualisant", 1200);
        }
    } else {
        setCotisation("EPCIMutualisant", 1200);
    }
}